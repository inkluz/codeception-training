<?php
namespace Page\Getresponse;

use AcceptanceTester;

class DashboardPage
{
    public static $URL = 'https://app.getresponse.com/dashboard';
    public static $linkNavbarAccountMenu ='[data-ats-navbar="account_menu"]';
    public static $buttonLogOut = '[data-ats-navbar="account_menu_log_out"]';
    public static $buttonLogMeInAgain = '[data-ats-logout="login_link"]';

    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var AcceptanceTester
     */
    protected $tester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function LogOut()
    {
        $I = $this->tester;

        $I->waitForElementClickable(self::$linkNavbarAccountMenu);
        $I->click(self::$linkNavbarAccountMenu);

        $I->waitForElementClickable(self::$buttonLogOut);
        $I->click(self::$buttonLogOut);

        $I->waitForElementClickable(self::$buttonLogMeInAgain);
        $I->click(self::$buttonLogMeInAgain);

        $I->waitForElementNotVisible(self::$buttonLogMeInAgain);

        return $this;
    }
}
