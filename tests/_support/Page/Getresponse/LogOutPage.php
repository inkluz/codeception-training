<?php
namespace Page\Getresponse;

class LogOutPage
{
    public static $URL = 'https://app.getresponse.com/login';

    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var AcceptanceTester
     */
    protected $tester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;
    }
}
