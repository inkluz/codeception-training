<?php
namespace Page\Getresponse;

use AcceptanceTester;

class LoginPage
{
    public static $URL = 'https://app.getresponse.com/login';
    public static $inputLogin = '[data-ats-login-form="input_login"]';
    public static $inputPassword = '[data-ats-login-form="input_password"]';
    public static $buttonSubmit = '[data-ats-login-form="input_submit"]';
    public static $checkboxRememberMe = '[data-ats-login-form="checkbox_remember"]';

    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var AcceptanceTester
     */

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function logIn($username, $password)
    {
        $I = $this->tester;

        $I->waitForElementClickable(self::$inputLogin);
        $I->fillField(self::$inputLogin, $username);

        $I->waitForElementClickable(self::$inputPassword);
        $I->fillField(self::$inputPassword, $password);

        $I->waitForElementClickable(self::$buttonSubmit);
        $I->click(self::$buttonSubmit);

        return $this;
    }

    public function rememberMeCheckbox()
    {
        $I = $this->tester;

        $I->waitForElementClickable(LoginPage::$inputLogin);
        $I->fillField(LoginPage::$inputLogin, TEST_ACCOUNT_LOGIN);
        $I->waitForElementClickable(LoginPage::$inputPassword);
        $I->fillField(LoginPage::$inputPassword, TEST_ACCOUNT_PASSWORD);

        $I->click(LoginPage::$checkboxRememberMe);
        $I->waitForElementClickable(LoginPage::$buttonSubmit);
        $I->click(LoginPage::$buttonSubmit);

        return $this;
    }
}
