<?php

use Codeception\Example;
use Page\Getresponse\LoginPage;
use Page\Getresponse\DashboardPage;

class FirstCest
{
    /**
     * @dataprovider userLogInDataCorrect
     */
    public function testStandardUserLogInToGetresponse(
        AcceptanceTester $I, Example $example, LoginPage $loginPage, DashboardPage $dashboardPage)
    {
        $I->amOnUrl(LoginPage::$URL);
        $loginPage->logIn($example['login'], $example['password']);
        $I->wait(5);
        $I->seeInCurrentUrl(parse_url(DashboardPage::$URL, PHP_URL_PATH));
        $dashboardPage->LogOut();
    }

    protected function userLogInDataCorrect()
    {
        return [
            ['login' => 'grautotest+email_brl1@implix.com', 'password' => 'JwtqUitKGX]H3kTk'],
        ];
    }
}
