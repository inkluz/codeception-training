<?php

use Codeception\Example;
use Page\Getresponse\LoginPage;
use Page\Getresponse\DashboardPage;

class LogInMechanismWhenLoginOrPasswordAreInvalidCest
{
    /**
     * @dataprovider userLogInDataIncorrect
     */

    public function testInvalidUserLogInToGetresponse(
        AcceptanceTester $I, Example $example, LoginPage $loginPage, DashboardPage $dashboardPage)
    {
        $I->amOnUrl(LoginPage::$URL);
        $loginPage->logIn($example['login'], $example['password']);
        $I->wait(5);
        $I->seeInCurrentUrl(parse_url(LoginPage::$URL, PHP_URL_PATH));
        $dashboardPage->LogOut();
    }

    protected function userLogInDataIncorrect()
    {
        return [
            ['login' => 'grautotest+email_brl1@implix.comJwtqUitKGX]H3kTk', 'password' => '   '],
            ['login' => '    ', 'password' => 'rautotest+email_brl1@implix.comJwtqUitKGX]H3kTk'],
            ['login' => '    ', 'password' => '    '],
            ['login' => 'JwtqUitKGX]H3kTk', 'password' => 'rautotest+email_brl1@implix.com'],
            ['login' => 'grautotest+email_brl1@implix.com', 'password' => 'Abc123!@#'],
        ];
    }
}
